package language

import (
	"io/fs"
	"path/filepath"
)

const (
	EnglishLanguage    = "english"
	EspanolLanguage    = "espanol"
	PortugueseLanguage = "portuguese"
)

const (
	languagesDirectory = "/data files/data/language/"
)

func ScanLanguageFiles(dir string, language string) (files []string, err error) {
	err = filepath.Walk(filepath.Join(dir, languagesDirectory, language), func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		files = append(files, path)
		return err
	})
	return files, err
}
