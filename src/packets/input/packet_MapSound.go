package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketMapSound struct {
	V1 types.UShort
	V2 types.String
}

func (p *PacketMapSound) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadString()
}

func (p *PacketMapSound) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteString(p.V2)
}
