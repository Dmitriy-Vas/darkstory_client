package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketCouponReady struct {
	V1 types.Byte
	V2 types.Integer
}

func (p *PacketCouponReady) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadInteger()
}

func (p *PacketCouponReady) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteInteger(p.V2)
}
