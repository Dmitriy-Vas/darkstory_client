package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerTimer struct {
	V1 types.Integer
	V2 types.Byte
}

func (p *PacketPlayerTimer) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadByte()
}

func (p *PacketPlayerTimer) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteByte(p.V2)
}
