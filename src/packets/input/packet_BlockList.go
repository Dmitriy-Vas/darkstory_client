package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketBlockList struct {
	V1 types.String
	V2 types.Byte
	V3 types.String
}

func (p *PacketBlockList) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadString()
}

func (p *PacketBlockList) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteString(p.V3)
}
