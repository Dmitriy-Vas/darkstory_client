package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketNpcDead struct {
	V1 types.Byte
	V2 types.UShort
	V3 types.UShort
	V4 types.Byte
	V5 types.Byte
	V6 types.UShort
	V7 types.String
}

func (p *PacketNpcDead) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadByte()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadUShort()
	p.V7 = buf.ReadString()
}

func (p *PacketNpcDead) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteByte(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteUShort(p.V6)
	buf.WriteString(p.V7)
}
