package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSayMessage struct {
	V1 types.UShort
	V2 types.String
	V3 types.String
	V4 types.String
	V5 types.Boolean
	V6 types.Byte
	V7 types.Boolean
}

func (p *PacketSayMessage) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadString()
	p.V4 = buf.ReadString()
	p.V5 = buf.ReadBoolean()
	p.V6 = buf.ReadByte()
	p.V7 = buf.ReadBoolean()
}

func (p *PacketSayMessage) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteString(p.V2)
	buf.WriteString(p.V3)
	buf.WriteString(p.V4)
	buf.WriteBoolean(p.V5)
	buf.WriteByte(p.V6)
	buf.WriteBoolean(p.V7)
}
