package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketDailyCheck struct {
	V1 types.Date
	V2 types.Byte
	V3 types.Date
}

func (p *PacketDailyCheck) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadDate()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadDate()
}

func (p *PacketDailyCheck) Write(buf buffer.BufferWrite) {
	buf.WriteDate(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteDate(p.V3)
}
