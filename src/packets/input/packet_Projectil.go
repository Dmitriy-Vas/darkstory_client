package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketProjectil struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.UShort
	V4 types.Byte
	V5 types.Byte
}

func (p *PacketProjectil) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadByte()
	p.V5 = buf.ReadByte()
}

func (p *PacketProjectil) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteByte(p.V4)
	buf.WriteByte(p.V5)
}
