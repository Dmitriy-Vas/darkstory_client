package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketShowEmoticon struct {
	V1 types.Boolean
	V2 types.UShort
	V3 types.UShort
}

func (p *PacketShowEmoticon) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
}

func (p *PacketShowEmoticon) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
}
