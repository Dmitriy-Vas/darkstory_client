package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketServerMsg struct {
	V1 types.Short
	V2 types.String
	V3 types.String
}

func (p *PacketServerMsg) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadShort()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadString()
}

func (p *PacketServerMsg) Write(buf buffer.BufferWrite) {
	buf.WriteShort(p.V1)
	buf.WriteString(p.V2)
	buf.WriteString(p.V3)
}
