package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketDemoUpdateQuest struct {
	V1 types.Integer
	V2 types.Integer
	V3 types.Integer
	V4 types.Integer
	V5 types.Boolean
	V6 types.Boolean
}

func (p *PacketDemoUpdateQuest) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadInteger()
	p.V4 = buf.ReadInteger()
	p.V5 = buf.ReadBoolean()
	p.V6 = buf.ReadBoolean()
}

func (p *PacketDemoUpdateQuest) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteInteger(p.V3)
	buf.WriteInteger(p.V4)
	buf.WriteBoolean(p.V5)
	buf.WriteBoolean(p.V6)
}
