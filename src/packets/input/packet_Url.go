package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketUrl struct {
	V1 types.String
}

func (p *PacketUrl) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketUrl) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
