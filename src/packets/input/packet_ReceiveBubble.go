package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/helpers"
	"darkstory_client/src/types"
)

type PacketReceiveBubble struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.String
	V4 types.Vector2
	V5 types.UShort
	V6 types.String
	V7 types.StringArray
}

func (p *PacketReceiveBubble) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadString()
	p.V4 = buf.ReadVector2()
	p.V5 = buf.ReadUShort()
	p.V6 = buf.ReadString()
	if helpers.IsNumeric(p.V6) {
		p.V7 = buf.ReadStringArray()
	}
}

func (p *PacketReceiveBubble) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteString(p.V3)
	buf.WriteVector2(p.V4)
	buf.WriteUShort(p.V5)
	buf.WriteString(p.V6)
	if helpers.IsNumeric(p.V6) {
		buf.WriteStringArray(p.V7)
	}
}
