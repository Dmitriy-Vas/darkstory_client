package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketBuffInfo struct {
	V1 types.UShort
	V2 types.Boolean
	V3 types.Byte
	V4 types.UShort
	V5 types.Integer
	V6 types.Integer
}

func (p *PacketBuffInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadByte()
	if p.V2 {
		p.V4 = buf.ReadUShort()
		p.V5 = buf.ReadInteger()
		p.V6 = buf.ReadInteger()
	}
}

func (p *PacketBuffInfo) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteBoolean(p.V2)
	buf.WriteByte(p.V3)
	if p.V2 {
		buf.WriteUShort(p.V4)
		buf.WriteInteger(p.V5)
		buf.WriteInteger(p.V6)
	}
}
