package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerCurrent struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Byte
	V4 types.Byte
}

func (p *PacketPlayerCurrent) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadByte()
}

func (p *PacketPlayerCurrent) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteByte(p.V4)
}
