package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketOpenUI struct {
	V1 types.UShort
	V2 types.Integer
	V3 types.String
}

func (p *PacketOpenUI) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadString()
}

func (p *PacketOpenUI) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteString(p.V3)
}
