package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSkulls struct {
	V1 []types.Long
	V2 types.Boolean
	V3 types.Byte
	V4 types.Byte
	V5 types.Integer
}

func (p *PacketSkulls) Read(buf buffer.BufferRead) {
	p.V1 = make([]types.Long, 2)
	for i := range p.V1 {
		p.V1[i] = buf.ReadLong()
	}
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadByte()
	p.V5 = buf.ReadInteger()
}

func (p *PacketSkulls) Write(buf buffer.BufferWrite) {
	if len(p.V1) != 2 {
		panic("incorrect length")
	}
	for i := range p.V1 {
		buf.WriteLong(p.V1[i])
	}
	buf.WriteBoolean(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteByte(p.V4)
	buf.WriteInteger(p.V5)
}
