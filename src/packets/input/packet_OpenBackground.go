package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketOpenBackground struct {
	V1 types.UShort
}

func (p *PacketOpenBackground) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketOpenBackground) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
