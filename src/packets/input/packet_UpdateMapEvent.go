package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketUpdateMapEvent struct {
	V1  types.UShort
	V2  types.UShort
	V3  types.Byte
	V4  types.UShort
	V5  types.Byte
	V6  types.String
	V7  types.UShort
	V8  types.UShort
	V9  types.UShort
	V10 types.UShort
	V11 types.UShort
	V12 types.UShort
	V13 types.UShort
	V14 types.UShort
	V15 types.Boolean
}

func (p *PacketUpdateMapEvent) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadString()
	p.V7 = buf.ReadUShort()
	p.V8 = buf.ReadUShort()
	p.V9 = buf.ReadUShort()
	p.V10 = buf.ReadUShort()
	p.V11 = buf.ReadUShort()
	p.V12 = buf.ReadUShort()
	p.V13 = buf.ReadUShort()
	p.V14 = buf.ReadUShort()
	p.V15 = buf.ReadBoolean()
}

func (p *PacketUpdateMapEvent) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteString(p.V6)
	buf.WriteUShort(p.V7)
	buf.WriteUShort(p.V8)
	buf.WriteUShort(p.V9)
	buf.WriteUShort(p.V10)
	buf.WriteUShort(p.V11)
	buf.WriteUShort(p.V12)
	buf.WriteUShort(p.V13)
	buf.WriteUShort(p.V14)
	buf.WriteBoolean(p.V15)
}
