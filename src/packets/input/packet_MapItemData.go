package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketMapItemData struct {
	V1 types.UShort
	V2 types.UShort
}

func (p *PacketMapItemData) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
}

func (p *PacketMapItemData) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
}
