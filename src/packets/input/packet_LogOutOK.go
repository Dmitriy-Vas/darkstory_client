package input

import (
	"darkstory_client/src/buffer"
)

type PacketLogOutOK struct {
}

func (p *PacketLogOutOK) Read(_ buffer.BufferRead) {
}

func (p *PacketLogOutOK) Write(_ buffer.BufferWrite) {
}
