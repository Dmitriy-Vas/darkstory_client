package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSpawnNPC struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.UShort
	V4 types.UShort
	V5 types.Byte
	V6 types.Long
	V7 []types.Long
	V8 types.Byte
	V9 types.Boolean
}

func (p *PacketSpawnNPC) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadLong()
	p.V7 = make([]types.Long, 3)
	for i := range p.V7 {
		p.V7[i] = buf.ReadLong()
	}
	p.V8 = buf.ReadByte()
	p.V9 = buf.ReadBoolean()
}

func (p *PacketSpawnNPC) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteLong(p.V6)
	if len(p.V7) != 3 {
		panic("incorrect length")
	}
	for i := range p.V7 {
		buf.WriteLong(p.V7[i])
	}
	buf.WriteByte(p.V8)
	buf.WriteBoolean(p.V9)
}
