package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketNpcAttack struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.Boolean
	V4 types.UShort
	V5 types.Long
	V6 types.Long
}

func (p *PacketNpcAttack) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadBoolean()
	if p.V1 > 0 {
		p.V4 = buf.ReadUShort()
		p.V5 = buf.ReadLong()
		p.V6 = buf.ReadLong()
	}
}

func (p *PacketNpcAttack) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteBoolean(p.V3)
	if p.V1 > 0 {
		buf.WriteUShort(p.V4)
		buf.WriteLong(p.V5)
		buf.WriteLong(p.V6)
	}
}
