package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerDeath struct {
	V1 types.UShort
	V2 types.Byte
}

func (p *PacketPlayerDeath) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
}

func (p *PacketPlayerDeath) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
}
