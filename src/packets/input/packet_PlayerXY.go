package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerXY struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Byte
}

func (p *PacketPlayerXY) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadByte()
}

func (p *PacketPlayerXY) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteByte(p.V3)
}
