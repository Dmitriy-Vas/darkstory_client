package input

import (
	"darkstory_client/src/buffer"
)

type PacketCharacterConfirm struct {
}

func (p *PacketCharacterConfirm) Read(_ buffer.BufferRead) {
}

func (p *PacketCharacterConfirm) Write(_ buffer.BufferWrite) {
}
