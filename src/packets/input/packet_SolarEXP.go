package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSolarEXP struct {
	V1 types.Byte
	V2 types.UShort
}

func (p *PacketSolarEXP) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
}

func (p *PacketSolarEXP) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
}
