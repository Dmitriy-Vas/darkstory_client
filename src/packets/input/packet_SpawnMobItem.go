package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSpawnMobItem struct {
	V1 types.Byte
	V2 types.UShort
	V3 types.Long
	V4 types.UShort
	V5 types.UShort
	V6 types.UShort
	V7 types.UShort
	V8 types.UShort
}

func (p *PacketSpawnMobItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	if p.V1 == 0 {
		return
	}
	p.V2 = buf.ReadUShort()
	if p.V2 > 0 {
		p.V3 = buf.ReadLong()
		p.V4 = buf.ReadUShort()
		p.V5 = buf.ReadUShort()
		p.V6 = buf.ReadUShort()
		p.V7 = buf.ReadUShort()
		p.V8 = buf.ReadUShort()
	}
}

func (p *PacketSpawnMobItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	if p.V1 == 0 {
		return
	}
	buf.WriteUShort(p.V2)
	if p.V2 > 0 {
		buf.WriteLong(p.V3)
		buf.WriteUShort(p.V4)
		buf.WriteUShort(p.V5)
		buf.WriteUShort(p.V6)
		buf.WriteUShort(p.V7)
		buf.WriteUShort(p.V8)
	}
}
