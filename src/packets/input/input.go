package input

import (
	"darkstory_client/src/buffer"
	"sync"
)

var (
	// Кэш пакетов
	packetsCache InputPackets
	mutex        *sync.RWMutex
)

// Packet описывает пакет и методы для IO взаимодействия
type Packet interface {
	Read(buf buffer.BufferRead)
	Write(buf buffer.BufferWrite)
}

// InputPackets описывает хранилище для пакетов по их ID или типу
type InputPackets struct {
	idsMap   map[int]Packet
	typesMap map[interface{}]Packet
}

// GetPacketById возвращает сохранённый пакет по ID
func (p InputPackets) GetPacketById(id int) Packet {
	return p.idsMap[id]
}

// GetPacketByType возвращает сохранённый пакет с подходящим типом
func (p InputPackets) GetPacketByType(t interface{}) Packet {
	return p.typesMap[t]
}

// GetPackets создаёт копию из сохранённых пакетов
func GetPackets() InputPackets {
	mutex.RLock()
	defer mutex.RUnlock()
	packetsCopy := newPackets()
	for key, value := range packetsCache.idsMap {
		packetsCopy.idsMap[key] = value
	}
	for key, value := range packetsCache.typesMap {
		packetsCopy.typesMap[key] = value
	}
	return packetsCopy
}

// При каждом запуске создаётся копия из сохранённых пакетов
func init() {
	packetsCache = newPackets()
	mutex = new(sync.RWMutex)
	registerIds()
	registerTypes()
}

// newPackets возвращает новый экземпляр packets
func newPackets() InputPackets {
	return InputPackets{
		idsMap:   make(map[int]Packet),
		typesMap: make(map[interface{}]Packet),
	}
}

// registerIds регистрирует пакеты в соответствии с их ID
func registerIds() {
	mutex.Lock()
	defer mutex.Unlock()

	packetsCache.idsMap[AlertMSG] = (*PacketAlertMSG)(nil)
	packetsCache.idsMap[LoginOk] = (*PacketLoginOk)(nil)
	packetsCache.idsMap[InGame] = (*PacketInGame)(nil)
	packetsCache.idsMap[Coins] = (*PacketCoins)(nil)
	packetsCache.idsMap[Skulls] = (*PacketSkulls)(nil)
	packetsCache.idsMap[PlayerStats] = (*PacketPlayerStats)(nil)
	packetsCache.idsMap[NpcMove] = (*PacketNpcMove)(nil)
	packetsCache.idsMap[PlayerDir] = (*PacketPlayerDir)(nil)
	packetsCache.idsMap[NpcDir] = (*PacketNpcDir)(nil)
	packetsCache.idsMap[PlayerXY] = (*PacketPlayerXY)(nil)
	packetsCache.idsMap[Attack] = (*PacketAttack)(nil)
	packetsCache.idsMap[NpcAttack] = (*PacketNpcAttack)(nil)
	packetsCache.idsMap[CheckMap] = (*PacketCheckMap)(nil)
	packetsCache.idsMap[MapItemData] = (*PacketMapItemData)(nil)
	packetsCache.idsMap[SteamAchievement] = (*PacketSteamAchievement)(nil)
	packetsCache.idsMap[GlobalMessage] = (*PacketGlobalMessage)(nil)
	packetsCache.idsMap[MapMessage] = (*PacketMapMessage)(nil)
	packetsCache.idsMap[SpawnItem] = (*PacketSpawnItem)(nil)
	packetsCache.idsMap[SpawnNPC] = (*PacketSpawnNPC)(nil)
	packetsCache.idsMap[NpcDead] = (*PacketNpcDead)(nil)
	packetsCache.idsMap[PlayerTimer] = (*PacketPlayerTimer)(nil)
	packetsCache.idsMap[ActionMessage] = (*PacketActionMessage)(nil)
	packetsCache.idsMap[PlayerSprite] = (*PacketPlayerSprite)(nil)
	packetsCache.idsMap[Animation] = (*PacketAnimation)(nil)
	packetsCache.idsMap[Cooldown] = (*PacketCooldown)(nil)
	packetsCache.idsMap[ClearSpellBuffer] = (*PacketClearSpellBuffer)(nil)
	packetsCache.idsMap[SayMessage] = (*PacketSayMessage)(nil)
	packetsCache.idsMap[Stunned] = (*PacketStunned)(nil)
	packetsCache.idsMap[PlayerCurrent] = (*PacketPlayerCurrent)(nil)
	packetsCache.idsMap[Close] = (*PacketClose)(nil)
	packetsCache.idsMap[TradeStatus] = (*PacketTradeStatus)(nil)
	packetsCache.idsMap[HighIndex] = (*PacketHighIndex)(nil)
	packetsCache.idsMap[Target] = (*PacketTarget)(nil)
	packetsCache.idsMap[Defensa] = (*PacketDefensa)(nil)
	packetsCache.idsMap[SendTime] = (*PacketSendTime)(nil)
	packetsCache.idsMap[SolarEXP] = (*PacketSolarEXP)(nil)
	packetsCache.idsMap[PlayerInvite] = (*PacketPlayerInvite)(nil)
	packetsCache.idsMap[BossMsg] = (*PacketBossMsg)(nil)
	packetsCache.idsMap[PlayerXYMap] = (*PacketPlayerXYMap)(nil)
	packetsCache.idsMap[Projectil] = (*PacketProjectil)(nil)
	packetsCache.idsMap[Flash] = (*PacketFlash)(nil)
	packetsCache.idsMap[RequestEditor] = (*PacketRequestEditor)(nil)
	packetsCache.idsMap[PlayerCards] = (*PacketPlayerCards)(nil)
	packetsCache.idsMap[PlayerCrafting] = (*PacketPlayerCrafting)(nil)
	packetsCache.idsMap[PlayerDeath] = (*PacketPlayerDeath)(nil)
	packetsCache.idsMap[BuffInfo] = (*PacketBuffInfo)(nil)
	packetsCache.idsMap[SpellAnim] = (*PacketSpellAnim)(nil)
	packetsCache.idsMap[MapSound] = (*PacketMapSound)(nil)
	packetsCache.idsMap[Bandit] = (*PacketBandit)(nil)
	packetsCache.idsMap[PVP] = (*PacketPVP)(nil)
	packetsCache.idsMap[ServerMsg] = (*PacketServerMsg)(nil)
	packetsCache.idsMap[DailyCheck] = (*PacketDailyCheck)(nil)
	packetsCache.idsMap[SpawnMobItem] = (*PacketSpawnMobItem)(nil)
	packetsCache.idsMap[ReceiveHour] = (*PacketReceiveHour)(nil)
	packetsCache.idsMap[ReceiveBubble] = (*PacketReceiveBubble)(nil)
	packetsCache.idsMap[ClearChoice] = (*PacketClearChoice)(nil)
	packetsCache.idsMap[BreakPoint] = (*PacketBreakPoint)(nil)
	packetsCache.idsMap[UpdateMapEvent] = (*PacketUpdateMapEvent)(nil)
	packetsCache.idsMap[SendConditionVar] = (*PacketSendConditionVar)(nil)
	packetsCache.idsMap[SendPlayerVars] = (*PacketSendPlayerVars)(nil)
	packetsCache.idsMap[QuestMsg] = (*PacketQuestMsg)(nil)
	packetsCache.idsMap[UpdatePlayerList] = (*PacketUpdatePlayerList)(nil)
	packetsCache.idsMap[Tutorial] = (*PacketTutorial)(nil)
	packetsCache.idsMap[Earthquake] = (*PacketEarthquake)(nil)
	packetsCache.idsMap[TravelInfo] = (*PacketTravelInfo)(nil)
	packetsCache.idsMap[LoginBackOK] = (*PacketLoginBackOK)(nil)
	packetsCache.idsMap[PlayerAwards] = (*PacketPlayerAwards)(nil)
	packetsCache.idsMap[CharacterConfirm] = (*PacketCharacterConfirm)(nil)
	packetsCache.idsMap[OpenUI] = (*PacketOpenUI)(nil)
	packetsCache.idsMap[LogOutOK] = (*PacketLogOutOK)(nil)
	packetsCache.idsMap[PlayerScore] = (*PacketPlayerScore)(nil)
	packetsCache.idsMap[PartyQuestInfo] = (*PacketPartyQuestInfo)(nil)
	packetsCache.idsMap[DemoUpdateQuest] = (*PacketDemoUpdateQuest)(nil)
	packetsCache.idsMap[CashInvUpdate] = (*PacketCashInvUpdate)(nil)
	packetsCache.idsMap[PlayerHonor] = (*PacketPlayerHonor)(nil)
	packetsCache.idsMap[DrillBreak] = (*PacketDrillBreak)(nil)
	packetsCache.idsMap[ArenaBonus] = (*PacketArenaBonus)(nil)
	packetsCache.idsMap[NpcBuffInfo] = (*PacketNpcBuffInfo)(nil)
	packetsCache.idsMap[Announcer] = (*PacketAnnouncer)(nil)
	packetsCache.idsMap[OpenBackground] = (*PacketOpenBackground)(nil)
	packetsCache.idsMap[CouponReady] = (*PacketCouponReady)(nil)
	packetsCache.idsMap[ProfessionStart] = (*PacketProfessionStart)(nil)
	packetsCache.idsMap[PlayerMaster] = (*PacketPlayerMaster)(nil)
	packetsCache.idsMap[Url] = (*PacketUrl)(nil)
	packetsCache.idsMap[PlayerStatus] = (*PacketPlayerStatus)(nil)
	packetsCache.idsMap[PlayerConnected] = (*PacketPlayerConnected)(nil)
	packetsCache.idsMap[PlayerTrap] = (*PacketPlayerTrap)(nil)
	packetsCache.idsMap[ShowEmoticon] = (*PacketShowEmoticon)(nil)
	packetsCache.idsMap[ServerVars] = (*PacketServerVars)(nil)
	packetsCache.idsMap[BlockList] = (*PacketBlockList)(nil)
	packetsCache.idsMap[ConstantData] = (*PacketConstantData)(nil)
	packetsCache.idsMap[UpdatePlayerProfile] = (*PacketUpdatePlayerProfile)(nil)
}

// registerTypes регистрирует пакеты в соответствии с их типом
func registerTypes() {
	// TODO fill
}

// Список идентификаторов пакетов
const (
	AlertMSG            = 1
	LoginOk             = 2
	ClassesData         = 3
	InGame              = 4
	PlayerInvUpdate     = 5
	PlayerWornEquipment = 6
	PlayerVitals        = 7
	Coins               = 8
	Skulls              = 9
	PlayerStats         = 10
	PlayerData          = 11
	PlayerMove          = 12
	NpcMove             = 13
	PlayerDir           = 14
	NpcDir              = 15
	PlayerXY            = 16
	Attack              = 17
	NpcAttack           = 18
	CheckMap            = 19
	MapData             = 20
	MapItemData         = 21
	MapNPCData          = 22
	SteamAchievement    = 23
	GlobalMessage       = 24
	PlayerMessage       = 25
	MapMessage          = 26
	SpawnItem           = 27
	UpdateItem          = 28
	SpawnNPC            = 29
	NpcDead             = 30
	UpdateNPC           = 31
	UpdateShop          = 32
	UpdateSpell         = 33
	Spells              = 34
	PlayerTimer         = 35
	ResourceCache       = 36
	UpdateResource      = 37
	Ping                = 38
	ActionMessage       = 39
	PlayerExp           = 40
	PlayerSprite        = 41
	UpdateAnimation     = 42
	Animation           = 43
	MapNpcVitals        = 44
	Cooldown            = 45
	ClearSpellBuffer    = 46
	SayMessage          = 47
	OpenShop            = 48
	InspectPlayer       = 49
	Stunned             = 50
	MapWornEquipment    = 51
	Bank                = 52
	UpdateNewspaper     = 53
	PlayerCurrent       = 54
	Close               = 55
	TradeUpdate         = 56
	TradeStatus         = 57
	GameData            = 58
	HighIndex           = 59
	Target              = 60
	CharData            = 61
	Defensa             = 62
	UpdateQuest         = 63
	PlayerQuest         = 64
	SendTime            = 65
	SolarEXP            = 66
	PlayerInvite        = 67
	PartyUpdate         = 68
	PartyVitals         = 69
	BossMsg             = 70
	PlayerXYMap         = 71
	Projectil           = 72
	UpdateProjectil     = 73
	UpdateMoral         = 74
	Flash               = 75
	RequestEditor       = 76
	UpdateCard          = 77
	PlayerCards         = 78
	UpdateCrafting      = 79
	PlayerCrafting      = 80
	PlayerDeath         = 81
	UpdateTitle         = 82
	PlayerDataType      = 83
	BuffInfo            = 84
	SpellAnim           = 85
	BattleMsg           = 86
	NPCProjectil        = 87
	MapSound            = 88
	Arrow               = 89
	Bandit              = 90
	PlayerGameData      = 91
	PVP                 = 92
	MapTopoData         = 93
	TopoDrill           = 94
	UpdateCondition     = 95
	ServerMsg           = 96
	DailyCheck          = 97
	SpawnMobItem        = 98
	OpenPlayerStore     = 99
	ReceiveHour         = 100
	ReceiveBubble       = 101
	Choice              = 102
	ClearChoice         = 103
	GetAdminInfo        = 104
	BreakPoint          = 105
	UpdateMapEvent      = 106
	SendConditionVar    = 107
	SendPlayerVars      = 108
	QuestMsg            = 109
	UpdatePuzzle        = 110
	UpdatePuzzleCache   = 111
	UpdatePlayerList    = 112
	Combo               = 113
	Tutorial            = 114
	PlayerElement       = 115
	PlayerRank          = 116
	Earthquake          = 117
	EventRank           = 118
	TravelInfo          = 119
	LoginBackOK         = 120
	PlayerAwards        = 121
	Awards              = 122
	CharacterConfirm    = 123
	OpenUI              = 124
	LogOutOK            = 125
	PlayerScore         = 126
	PartyQuestInfo      = 127
	DemoUpdateQuest     = 128
	CashInvUpdate       = 129
	PlayerInfo          = 130
	PlayerHonor         = 131
	PlayerBuddies       = 132
	BuddiesAction       = 133
	DrillBreak          = 134
	ArenaBonus          = 136
	NpcBuffInfo         = 137
	Announcer           = 138
	OpenBackground      = 139
	CouponReady         = 140
	ProfessionStart     = 141
	Modifier            = 142
	PlayerMaster        = 143
	Url                 = 144
	PlayerStatus        = 145
	PlayerConnected     = 146
	ServerStats         = 147
	GetModInfo          = 148
	PlayerTrap          = 149
	ShowEmoticon        = 150
	ProfessionData      = 151
	ServerVars          = 152
	CashShopData        = 153
	BlockList           = 154
	ConstantData        = 155
	UpdatePlayerProfile = 156
)
