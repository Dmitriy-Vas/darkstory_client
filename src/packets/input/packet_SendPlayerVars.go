package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendPlayerVars struct {
	V1 types.Byte
	V2 []types.Boolean
	V3 types.Boolean
}

func (p *PacketSendPlayerVars) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	if p.V1 == 0 {
		p.V2 = make([]types.Boolean, 100)
		for i := range p.V2 {
			p.V2[i] = buf.ReadBoolean()
		}
	} else {
		p.V3 = buf.ReadBoolean()
	}
}

func (p *PacketSendPlayerVars) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	if p.V1 == 0 {
		if len(p.V2) != 100 {
			panic("incorrect length")
		}
		for i := range p.V2 {
			buf.WriteBoolean(p.V2[i])
		}
	} else {
		buf.WriteBoolean(p.V3)
	}
}
