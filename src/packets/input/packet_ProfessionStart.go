package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketProfessionStart struct {
	V1 types.Byte
	V2 types.Integer
	V3 types.Integer
	V4 types.Integer
}

func (p *PacketProfessionStart) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadInteger()
	p.V4 = buf.ReadInteger()
}

func (p *PacketProfessionStart) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteInteger(p.V3)
	buf.WriteInteger(p.V4)
}
