package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketNpcBuffInfo struct {
	V1 types.Byte
	V2 types.Byte
}

func (p *PacketNpcBuffInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadByte()
}

func (p *PacketNpcBuffInfo) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteByte(p.V2)
}
