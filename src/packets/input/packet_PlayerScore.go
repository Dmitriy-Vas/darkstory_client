package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerScore struct {
	V1 types.Integer
}

func (p *PacketPlayerScore) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
}

func (p *PacketPlayerScore) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
}
