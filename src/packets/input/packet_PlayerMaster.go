package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerMaster struct {
	V1 types.UShort
	V2 []types.UShort
}

func (p *PacketPlayerMaster) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = make([]types.UShort, 25)
	for i := range p.V2 {
		p.V2[i] = buf.ReadUShort()
	}
}

func (p *PacketPlayerMaster) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	if len(p.V2) != 25 {
		panic("incorrect length")
	}
	for i := range p.V2 {
		buf.WriteUShort(p.V2[i])
	}
}
