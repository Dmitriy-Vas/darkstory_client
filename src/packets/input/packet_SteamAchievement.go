package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSteamAchievement struct {
	V1 types.String
	V2 types.String
}

func (p *PacketSteamAchievement) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadString()
}

func (p *PacketSteamAchievement) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteString(p.V2)
}
