package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketQuestMsg struct {
	V1 types.String
	V2 types.String
	V3 types.Short
}

func (p *PacketQuestMsg) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadShort()
}

func (p *PacketQuestMsg) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteString(p.V2)
	buf.WriteShort(p.V3)
}
