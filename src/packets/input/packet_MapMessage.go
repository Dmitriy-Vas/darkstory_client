package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketMapMessage struct {
	V1 types.String
}

func (p *PacketMapMessage) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketMapMessage) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
