package input

import (
	"darkstory_client/src/buffer"
)

type PacketDrillBreak struct {
}

func (p *PacketDrillBreak) Read(_ buffer.BufferRead) {
}

func (p *PacketDrillBreak) Write(_ buffer.BufferWrite) {
}
