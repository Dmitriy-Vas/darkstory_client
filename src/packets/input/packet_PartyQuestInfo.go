package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPartyQuestInfo struct {
	V1 types.Integer
}

func (p *PacketPartyQuestInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
}

func (p *PacketPartyQuestInfo) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
}
