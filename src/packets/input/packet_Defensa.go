package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketDefensa struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Boolean
}

func (p *PacketDefensa) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadBoolean()
}

func (p *PacketDefensa) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteBoolean(p.V3)
}
