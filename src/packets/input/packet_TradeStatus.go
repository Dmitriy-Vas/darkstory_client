package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketTradeStatus struct {
	V1 types.String
	V2 types.Byte
}

func (p *PacketTradeStatus) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadByte()
}

func (p *PacketTradeStatus) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteByte(p.V2)
}
