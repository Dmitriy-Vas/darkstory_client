package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerCrafting struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Boolean
	V4 types.Boolean
}

func (p *PacketPlayerCrafting) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadBoolean()
	p.V4 = buf.ReadBoolean()
}

func (p *PacketPlayerCrafting) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteBoolean(p.V3)
	buf.WriteBoolean(p.V4)
}
