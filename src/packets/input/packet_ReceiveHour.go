package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketReceiveHour struct {
	V1 types.Integer
	V2 types.Byte
	V3 types.Byte
}

func (p *PacketReceiveHour) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadByte()
}

func (p *PacketReceiveHour) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteByte(p.V3)
}
