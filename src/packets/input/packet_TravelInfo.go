package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketTravelInfo struct {
	V1 types.Boolean
}

func (p *PacketTravelInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketTravelInfo) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
