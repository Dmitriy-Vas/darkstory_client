package input

import (
	"darkstory_client/src/buffer"
)

type PacketAnnouncer struct {
}

func (p *PacketAnnouncer) Read(_ buffer.BufferRead) {
}

func (p *PacketAnnouncer) Write(_ buffer.BufferWrite) {
}
