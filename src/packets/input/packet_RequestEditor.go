package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketRequestEditor struct {
	V1 types.UShort
}

func (p *PacketRequestEditor) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketRequestEditor) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
