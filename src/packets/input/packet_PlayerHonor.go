package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerHonor struct {
	V1 types.UShort
	V2 types.Integer
	V3 types.Date
}

func (p *PacketPlayerHonor) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadDate()
}

func (p *PacketPlayerHonor) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteDate(p.V3)
}
