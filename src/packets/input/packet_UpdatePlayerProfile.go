package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketUpdatePlayerProfile struct {
	V1 types.Byte
	V2 []types.UShort
}

func (p *PacketUpdatePlayerProfile) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = make([]types.UShort, 7)
	for i := range p.V2 {
		p.V2[i] = buf.ReadUShort()
	}
}

func (p *PacketUpdatePlayerProfile) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	if len(p.V2) != 7 {
		panic("incorrect length")
	}
	for i := range p.V2 {
		buf.WriteUShort(p.V2[i])
	}
}
