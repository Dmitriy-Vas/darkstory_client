package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketActionMessage struct {
	V1  types.Byte
	V2  types.UShort
	V3  types.UShort
	V4  types.String
	V5  types.Byte
	V6  types.Byte
	V7  types.String
	V8  types.Byte
	V9  types.Byte
	V10 types.Boolean
	V11 types.Byte
	V12 types.Byte
}

func (p *PacketActionMessage) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadString()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadByte()
	p.V7 = buf.ReadString()
	p.V8 = buf.ReadByte()
	p.V9 = buf.ReadByte()
	p.V10 = buf.ReadBoolean()
	p.V11 = buf.ReadByte()
	p.V12 = buf.ReadByte()
}

func (p *PacketActionMessage) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteString(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteByte(p.V6)
	buf.WriteString(p.V7)
	buf.WriteByte(p.V8)
	buf.WriteByte(p.V9)
	buf.WriteBoolean(p.V10)
	buf.WriteByte(p.V11)
	buf.WriteByte(p.V12)
}
