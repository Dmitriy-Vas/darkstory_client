package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketCooldown struct {
	V1 types.Byte
	V2 types.UShort
	V3 types.UShort
	V4 types.Boolean
}

func (p *PacketCooldown) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadBoolean()
}

func (p *PacketCooldown) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteBoolean(p.V4)
}
