package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketStunned struct {
	V1 types.Integer
	V2 types.Boolean
}

func (p *PacketStunned) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadBoolean()
}

func (p *PacketStunned) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteBoolean(p.V2)
}
