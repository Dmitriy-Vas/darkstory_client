package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerXYMap struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.UShort
	V4 types.Byte
}

func (p *PacketPlayerXYMap) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadByte()
}

func (p *PacketPlayerXYMap) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteByte(p.V4)
}
