package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketAlertMSG struct {
	V1  types.String
	V2  types.Boolean
	V3  types.Short
	V4  types.Boolean
	_V5 types.SByte
	V6  []types.String
}

func (p *PacketAlertMSG) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadShort()
	p.V4 = buf.ReadBoolean()
	p._V5 = buf.ReadSByte()
	if p._V5 > -1 {
		p.V6 = make([]types.String, p._V5+1)
		for i := range p.V6 {
			p.V6[i] = buf.ReadString()
		}
	}
}

func (p *PacketAlertMSG) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteBoolean(p.V2)
	buf.WriteShort(p.V3)
	buf.WriteBoolean(p.V4)
	p._V5 = types.SByte(len(p.V6) - 1)
	buf.WriteSByte(p._V5)
	if p._V5 > -1 {
		for i := range p.V6 {
			buf.WriteString(p.V6[i])
		}
	}
}
