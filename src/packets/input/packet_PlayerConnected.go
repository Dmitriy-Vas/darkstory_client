package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerConnected struct {
	V1 types.Integer
	V2 types.Boolean
	V3 []types.String
}

func (p *PacketPlayerConnected) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadBoolean()
	if p.V2 {
		p.V3 = make([]types.String, p.V1+1)
		for i := range p.V3 {
			p.V3[i] = buf.ReadString()
		}
	}
}

func (p *PacketPlayerConnected) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteBoolean(p.V2)
	if p.V2 {
		if types.Integer(len(p.V3)) != p.V1 {
			panic("incorrect length")
		}
		for i := range p.V3 {
			buf.WriteString(p.V3[i])
		}
	}
}
