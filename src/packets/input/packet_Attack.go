package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketAttack struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.String
}

func (p *PacketAttack) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadString()
}

func (p *PacketAttack) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteString(p.V3)
}
