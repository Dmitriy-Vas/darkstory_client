package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerStats struct {
	V1 types.UShort
	V2 []types.UShort
	V3 types.UShort
}

func (p *PacketPlayerStats) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = make([]types.UShort, 6)
	for i := range p.V2 {
		p.V2[i] = buf.ReadUShort()
	}
	p.V3 = buf.ReadUShort()
}

func (p *PacketPlayerStats) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	if len(p.V2) != 6 {
		panic("incorrect length")
	}
	for i := range p.V2 {
		buf.WriteUShort(p.V2[i])
	}
	buf.WriteUShort(p.V3)
}
