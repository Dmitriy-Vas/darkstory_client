package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendTime struct {
	V1 types.Boolean
	V2 types.String
}

func (p *PacketSendTime) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadString()
}

func (p *PacketSendTime) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteString(p.V2)
}
