package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketTutorial struct {
	V1 types.UShort
}

func (p *PacketTutorial) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketTutorial) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
