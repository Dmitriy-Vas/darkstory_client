package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketHighIndex struct {
	V1 types.UShort
}

func (p *PacketHighIndex) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketHighIndex) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
