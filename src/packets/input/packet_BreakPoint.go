package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketBreakPoint struct {
	V1 types.Integer
	V2 types.String
}

func (p *PacketBreakPoint) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadString()
}

func (p *PacketBreakPoint) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteString(p.V2)
}
