package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketNpcDir struct {
	V1 types.Byte
	V2 types.Byte
	V3 types.Boolean
	V4 types.UShort
	V5 types.UShort
}

func (p *PacketNpcDir) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadBoolean()
	if p.V3 {
		p.V4 = buf.ReadUShort()
		p.V5 = buf.ReadUShort()
	}
}

func (p *PacketNpcDir) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteBoolean(p.V3)
	if p.V3 {
		buf.WriteUShort(p.V4)
		buf.WriteUShort(p.V5)
	}
}
