package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSpawnItem struct {
	V1 types.Byte
	V2 types.Byte
	V3 types.UShort
	V4 types.UShort
	V5 types.Integer
	V6 types.UShort
	V7 types.UShort
}

func (p *PacketSpawnItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadUShort()
	if p.V2 == 0 {
		return
	}
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadInteger()
	p.V6 = buf.ReadUShort()
	p.V7 = buf.ReadUShort()
}

func (p *PacketSpawnItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteUShort(p.V3)
	if p.V2 == 0 {
		return
	}
	buf.WriteUShort(p.V4)
	buf.WriteInteger(p.V5)
	buf.WriteUShort(p.V6)
	buf.WriteUShort(p.V7)
}
