package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketBandit struct {
	V1 types.Byte
}

func (p *PacketBandit) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketBandit) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
