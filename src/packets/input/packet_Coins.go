package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketCoins struct {
	V1 types.Integer
}

func (p *PacketCoins) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
}

func (p *PacketCoins) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
}
