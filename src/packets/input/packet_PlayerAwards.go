package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerAwards struct {
	V1 types.UShort
	V2 types.Integer
	V3 types.UShort
	V4 types.Integer
	V5 types.String
	V6 types.Boolean
}

func (p *PacketPlayerAwards) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadInteger()
	p.V5 = buf.ReadString()
	p.V6 = buf.ReadBoolean()
}

func (p *PacketPlayerAwards) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteInteger(p.V4)
	buf.WriteString(p.V5)
	buf.WriteBoolean(p.V6)
}
