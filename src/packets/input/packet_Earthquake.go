package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketEarthquake struct {
	V1 types.Byte
}

func (p *PacketEarthquake) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketEarthquake) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
