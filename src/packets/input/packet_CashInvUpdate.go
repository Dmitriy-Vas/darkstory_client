package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketCashInvUpdate struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Long
}

func (p *PacketCashInvUpdate) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadLong()
}

func (p *PacketCashInvUpdate) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteLong(p.V3)
}
