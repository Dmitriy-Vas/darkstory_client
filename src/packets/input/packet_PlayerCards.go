package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerCards struct {
	V1 types.UShort
	V2 types.UShort
	V3 []types.UShort
	V4 types.UShort
}

func (p *PacketPlayerCards) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = make([]types.UShort, 255)
	for i := range p.V3 {
		p.V3[i] = buf.ReadUShort()
	}
	p.V4 = buf.ReadUShort()
}

func (p *PacketPlayerCards) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	if len(p.V3) != 255 {
		panic("incorrect length")
	}
	for i := range p.V3 {
		buf.WriteUShort(p.V3[i])
	}
	buf.WriteUShort(p.V4)
}
