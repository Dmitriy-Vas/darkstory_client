package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPVP struct {
	V1 types.Long
	V2 types.Long
}

func (p *PacketPVP) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadLong()
	p.V2 = buf.ReadLong()
}

func (p *PacketPVP) Write(buf buffer.BufferWrite) {
	buf.WriteLong(p.V1)
	buf.WriteLong(p.V2)
}
