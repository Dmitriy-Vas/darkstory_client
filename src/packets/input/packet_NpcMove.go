package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketNpcMove struct {
	V1  types.Byte
	V2  types.UShort
	V3  types.UShort
	V4  types.UShort
	V5  types.Byte
	V6  types.Byte
	V7  types.Byte
	V8  types.UShort
	V9  types.UShort
	V10 types.Integer
}

func (p *PacketNpcMove) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadByte()
	if p.V6 > 0 {
		p.V7 = buf.ReadByte()
		p.V8 = buf.ReadUShort()
		p.V9 = buf.ReadUShort()
		p.V10 = buf.ReadInteger()
	}
}

func (p *PacketNpcMove) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteByte(p.V6)
	if p.V6 > 0 {
		buf.WriteByte(p.V7)
		buf.WriteUShort(p.V8)
		buf.WriteUShort(p.V9)
		buf.WriteInteger(p.V10)
	}
}
