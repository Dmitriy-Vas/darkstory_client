package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketClearSpellBuffer struct {
	V1 types.Byte
}

func (p *PacketClearSpellBuffer) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketClearSpellBuffer) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
