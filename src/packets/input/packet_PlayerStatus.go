package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerStatus struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.Byte
	V4 types.Byte
	V5 types.UShort
	V6 types.UShort
	V7 types.Boolean
	V8 types.UShort
	V9 types.String
}

func (p *PacketPlayerStatus) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadByte()
	p.V5 = buf.ReadUShort()
	p.V6 = buf.ReadUShort()
	p.V7 = buf.ReadBoolean()
	p.V8 = buf.ReadUShort()
	p.V9 = buf.ReadString()
}

func (p *PacketPlayerStatus) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteByte(p.V4)
	buf.WriteUShort(p.V5)
	buf.WriteUShort(p.V6)
	buf.WriteBoolean(p.V7)
	buf.WriteUShort(p.V8)
	buf.WriteString(p.V9)
}
