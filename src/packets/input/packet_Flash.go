package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketFlash struct {
	V1 types.Byte
	V2 types.UShort
	V3 types.Byte
	V4 types.Boolean
	V5 types.UShort
}

func (p *PacketFlash) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadBoolean()
	p.V5 = buf.ReadUShort()
}

func (p *PacketFlash) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteBoolean(p.V4)
	buf.WriteUShort(p.V5)
}
