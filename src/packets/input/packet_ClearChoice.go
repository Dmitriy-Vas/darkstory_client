package input

import (
	"darkstory_client/src/buffer"
)

type PacketClearChoice struct {
}

func (p *PacketClearChoice) Read(_ buffer.BufferRead) {
}

func (p *PacketClearChoice) Write(_ buffer.BufferWrite) {
}
