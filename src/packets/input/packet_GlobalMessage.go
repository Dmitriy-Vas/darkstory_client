package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketGlobalMessage struct {
	V1 types.Byte
	V2 types.Short
	V3 types.String
	V4 types.String
}

func (p *PacketGlobalMessage) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadShort()
	p.V3 = buf.ReadString()
	p.V4 = buf.ReadString()
}

func (p *PacketGlobalMessage) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteShort(p.V2)
	buf.WriteString(p.V3)
	buf.WriteString(p.V4)
}
