package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketAnimation struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.UShort
	V4 types.UShort
	V5 types.String
	V6 types.Byte
	V7 types.UShort
	V8 types.UShort
}

func (p *PacketAnimation) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadString()
	p.V6 = buf.ReadByte()
	p.V7 = buf.ReadUShort()
	p.V8 = buf.ReadUShort()
}

func (p *PacketAnimation) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteString(p.V5)
	buf.WriteByte(p.V6)
	buf.WriteUShort(p.V7)
	buf.WriteUShort(p.V8)
}
