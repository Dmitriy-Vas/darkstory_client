package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketUpdatePlayerList struct {
	V1 types.UShort
	V2 types.SByte
}

func (p *PacketUpdatePlayerList) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadSByte()
}

func (p *PacketUpdatePlayerList) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteSByte(p.V2)
}
