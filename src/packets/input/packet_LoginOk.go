package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketLoginOk struct {
	V1 types.UShort
	V2 types.Byte
}

func (p *PacketLoginOk) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
}

func (p *PacketLoginOk) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
}
