package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketBossMsg struct {
	V1 types.UShort
	V2 types.String
	V3 types.String
}

func (p *PacketBossMsg) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadString()
}

func (p *PacketBossMsg) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteString(p.V2)
	buf.WriteString(p.V3)
}
