package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketServerVars struct {
	V1 types.Integer
}

func (p *PacketServerVars) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
}

func (p *PacketServerVars) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
}
