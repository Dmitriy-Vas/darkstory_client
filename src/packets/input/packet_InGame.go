package input

import (
	"darkstory_client/src/buffer"
)

type PacketInGame struct{}

func (p *PacketInGame) Read(_ buffer.BufferRead) {
}

func (p *PacketInGame) Write(_ buffer.BufferWrite) {
}
