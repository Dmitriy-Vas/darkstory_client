package input

import (
	"darkstory_client/src/buffer"
)

type PacketLoginBackOK struct {
}

func (p *PacketLoginBackOK) Read(_ buffer.BufferRead) {
}

func (p *PacketLoginBackOK) Write(_ buffer.BufferWrite) {
}
