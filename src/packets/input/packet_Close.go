package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketClose struct {
	V1 types.UShort
	V2 types.Integer
	V3 types.String
}

func (p *PacketClose) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadString()
}

func (p *PacketClose) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteString(p.V3)
}
