package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerSprite struct {
	V1 types.UShort
	V2 types.UShort
	V3 types.UShort
}

func (p *PacketPlayerSprite) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
}

func (p *PacketPlayerSprite) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
}
