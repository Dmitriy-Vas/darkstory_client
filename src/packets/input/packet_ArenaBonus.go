package input

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketArenaBonus struct {
	V1 types.Boolean
	V2 types.Byte
}

func (p *PacketArenaBonus) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadByte()
}

func (p *PacketArenaBonus) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteByte(p.V2)
}
