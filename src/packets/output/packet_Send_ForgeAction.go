package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_ForgeAction struct {
	V1 types.Byte
}

func (p *PacketSend_ForgeAction) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSend_ForgeAction) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
