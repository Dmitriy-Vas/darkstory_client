package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendPVP struct {
	V1 types.Long
	V2 types.Long
}

func (p *PacketSendPVP) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadLong()
	p.V2 = buf.ReadLong()
}

func (p *PacketSendPVP) Write(buf buffer.BufferWrite) {
	buf.WriteLong(p.V1)
	buf.WriteLong(p.V2)
}
