package output

import (
	"darkstory_client/src/buffer"
)

type PacketAcceptTrade struct {
}

func (p *PacketAcceptTrade) Read(_ buffer.BufferRead) {
}

func (p *PacketAcceptTrade) Write(_ buffer.BufferWrite) {
}
