package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendCashShopUI struct {
	V1 types.Boolean
}

func (p *PacketSendCashShopUI) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketSendCashShopUI) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
