package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Crafting struct {
	V1 types.UShort
	V2 types.UShort
}

func (p *PacketSend_Crafting) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadUShort()
}

func (p *PacketSend_Crafting) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteUShort(p.V2)
}
