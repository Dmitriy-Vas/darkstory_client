package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendUnequip struct {
	V1 types.Boolean
	V2 types.Byte
}

func (p *PacketSendUnequip) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadByte()
}

func (p *PacketSendUnequip) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteByte(p.V2)
}
