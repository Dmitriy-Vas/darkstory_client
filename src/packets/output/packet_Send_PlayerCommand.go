package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_PlayerCommand struct {
	V1 types.String
	V2 types.UShort
	V3 types.String
}

func (p *PacketSend_PlayerCommand) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadString()
}

func (p *PacketSend_PlayerCommand) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteString(p.V3)
}
