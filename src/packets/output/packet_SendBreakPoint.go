package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendBreakPoint struct {
	V1 types.UShort
}

func (p *PacketSendBreakPoint) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketSendBreakPoint) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
