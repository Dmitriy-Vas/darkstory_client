package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Whisper struct {
	V1 types.String
	V2 types.String
}

func (p *PacketSend_Whisper) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadString()
}

func (p *PacketSend_Whisper) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteString(p.V2)
}
