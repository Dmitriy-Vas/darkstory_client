package output

import (
	"darkstory_client/src/buffer"
)

type PacketSend_CloseBank struct {
}

func (p *PacketSend_CloseBank) Read(_ buffer.BufferRead) {
}

func (p *PacketSend_CloseBank) Write(_ buffer.BufferWrite) {
}
