package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_TrainStat struct {
	V1 types.Byte
	V2 types.Boolean
	V3 types.Integer
	V4 types.Byte
}

func (p *PacketSend_TrainStat) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadInteger()
	if !p.V2 {
		p.V4 = buf.ReadByte()
	}
}

func (p *PacketSend_TrainStat) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteBoolean(p.V2)
	buf.WriteInteger(p.V3)
	if !p.V2 {
		buf.WriteByte(p.V4)
	}
}
