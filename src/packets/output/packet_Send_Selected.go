package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Selected struct {
	V1 types.Byte
}

func (p *PacketSend_Selected) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSend_Selected) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
