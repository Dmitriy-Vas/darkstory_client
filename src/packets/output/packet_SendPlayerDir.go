package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendPlayerDir struct {
	V1 types.Byte
}

func (p *PacketSendPlayerDir) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendPlayerDir) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
