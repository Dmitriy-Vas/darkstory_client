package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendUseItem struct {
	V1 types.Byte
}

func (p *PacketSendUseItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendUseItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
