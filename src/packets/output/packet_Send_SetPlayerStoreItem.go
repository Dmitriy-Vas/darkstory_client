package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SetPlayerStoreItem struct {
	V1 types.Byte
	V2 types.Boolean
	V3 types.Byte
	V4 types.UShort
	V5 types.UInteger
}

func (p *PacketSend_SetPlayerStoreItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadBoolean()
	if p.V2 {
		p.V3 = buf.ReadByte()
		p.V4 = buf.ReadUShort()
		p.V5 = buf.ReadUInteger()
	}
}

func (p *PacketSend_SetPlayerStoreItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteBoolean(p.V2)
	if p.V2 {
		buf.WriteByte(p.V3)
		buf.WriteUShort(p.V4)
		buf.WriteUInteger(p.V5)
	}
}
