package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_TakeCapture struct {
	V1 types.Vector2
	V2 types.Integer
	V3 types.Byte
}

func (p *PacketSend_TakeCapture) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadVector2()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadByte()
}

func (p *PacketSend_TakeCapture) Write(buf buffer.BufferWrite) {
	buf.WriteVector2(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteByte(p.V3)
}
