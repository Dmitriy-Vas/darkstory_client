package output

import (
	"darkstory_client/src/buffer"
)

type PacketDeclineTrade struct {
}

func (p *PacketDeclineTrade) Read(_ buffer.BufferRead) {
}

func (p *PacketDeclineTrade) Write(_ buffer.BufferWrite) {
}
