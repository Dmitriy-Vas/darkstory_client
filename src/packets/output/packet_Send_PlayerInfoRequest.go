package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_PlayerInfoRequest struct {
	V1 types.String
}

func (p *PacketSend_PlayerInfoRequest) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketSend_PlayerInfoRequest) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
