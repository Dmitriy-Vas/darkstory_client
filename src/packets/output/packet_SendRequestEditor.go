package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendRequestEditor struct {
	V1 types.UShort
}

func (p *PacketSendRequestEditor) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketSendRequestEditor) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
