package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Hotkeys struct {
	V1 []types.Integer
}

func (p *PacketSend_Hotkeys) Read(buf buffer.BufferRead) {
	p.V1 = make([]types.Integer, 60)
	for i := range p.V1 {
		p.V1[i] = buf.ReadInteger()
	}
}

func (p *PacketSend_Hotkeys) Write(buf buffer.BufferWrite) {
	if len(p.V1) != 60 {
		panic("incorrect length")
	}
	for i := range p.V1 {
		buf.WriteInteger(p.V1[i])
	}
}
