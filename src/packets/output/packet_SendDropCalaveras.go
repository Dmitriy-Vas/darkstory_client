package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendDropCalaveras struct {
	V1 types.Byte
	V2 types.Long
}

func (p *PacketSendDropCalaveras) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadLong()
}

func (p *PacketSendDropCalaveras) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteLong(p.V2)
}
