package output

import (
	"darkstory_client/src/buffer"
)

type PacketSend_LogOut struct {
}

func (p *PacketSend_LogOut) Read(_ buffer.BufferRead) {
}

func (p *PacketSend_LogOut) Write(_ buffer.BufferWrite) {
}
