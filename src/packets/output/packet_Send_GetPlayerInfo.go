package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_GetPlayerInfo struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.Byte
}

func (p *PacketSend_GetPlayerInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadByte()
}

func (p *PacketSend_GetPlayerInfo) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteByte(p.V3)
}
