package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketWarpToMe struct {
	V1 types.String
}

func (p *PacketWarpToMe) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketWarpToMe) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
