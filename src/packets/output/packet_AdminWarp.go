package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketAdminWarp struct {
	V1 types.Byte
	V2 types.Byte
}

func (p *PacketAdminWarp) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadByte()
}

func (p *PacketAdminWarp) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteByte(p.V2)
}
