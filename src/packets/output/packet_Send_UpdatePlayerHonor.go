package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_UpdatePlayerHonor struct {
	V1 types.String
	V2 types.UShort
}

func (p *PacketSend_UpdatePlayerHonor) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadUShort()
}

func (p *PacketSend_UpdatePlayerHonor) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteUShort(p.V2)
}
