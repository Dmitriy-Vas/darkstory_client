package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Button struct {
	V1 types.UShort
	V2 types.Byte
	V3 types.Integer
	V4 types.UShort
	V5 types.String
}

func (p *PacketSend_Button) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadInteger()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadString()
}

func (p *PacketSend_Button) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteInteger(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteString(p.V5)
}
