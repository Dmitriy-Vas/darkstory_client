package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_NeedMap struct {
	V1 types.Boolean
}

func (p *PacketSend_NeedMap) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketSend_NeedMap) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
