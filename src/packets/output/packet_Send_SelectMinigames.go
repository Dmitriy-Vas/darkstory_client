package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SelectMinigames struct {
	V1 types.Byte
}

func (p *PacketSend_SelectMinigames) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSend_SelectMinigames) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
