package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketCheckDefensa struct {
	V1 types.Boolean
}

func (p *PacketCheckDefensa) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketCheckDefensa) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
