package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Search struct {
	V1 types.Byte
	V2 types.UShort
	V3 types.UShort
	V4 types.UShort
}

func (p *PacketSend_Search) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadUShort()
	p.V4 = buf.ReadUShort()
}

func (p *PacketSend_Search) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteUShort(p.V3)
	buf.WriteUShort(p.V4)
}
