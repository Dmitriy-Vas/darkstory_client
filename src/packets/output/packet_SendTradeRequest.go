package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendTradeRequest struct {
	V1 types.UShort
}

func (p *PacketSendTradeRequest) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketSendTradeRequest) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
