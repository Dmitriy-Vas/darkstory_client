package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendRequestNewChar struct {
	V1 types.Byte
}

func (p *PacketSendRequestNewChar) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendRequestNewChar) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
