package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_DragItem struct {
	V1 types.UShort
	V2 types.Boolean
	V3 types.Byte
	V4 types.Byte
}

func (p *PacketSend_DragItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadByte()
}

func (p *PacketSend_DragItem) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteBoolean(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteByte(p.V4)
}
