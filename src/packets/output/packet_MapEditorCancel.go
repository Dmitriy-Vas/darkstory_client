package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketMapEditorCancel struct {
	V1 types.Boolean
}

func (p *PacketMapEditorCancel) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketMapEditorCancel) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
