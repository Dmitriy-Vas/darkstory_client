package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendTryOpenUI struct {
}

func (p *PacketSendTryOpenUI) Read(_ buffer.BufferRead) {
}

func (p *PacketSendTryOpenUI) Write(_ buffer.BufferWrite) {
}
