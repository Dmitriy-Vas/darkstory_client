package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketTradeItem struct {
	V1 types.Byte
	V2 types.Long
	V3 types.Boolean
}

func (p *PacketTradeItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadLong()
	p.V3 = buf.ReadBoolean()
}

func (p *PacketTradeItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteLong(p.V2)
	buf.WriteBoolean(p.V3)
}
