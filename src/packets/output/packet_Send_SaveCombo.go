package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SaveCombo struct {
	V1 []types.UShort
}

func (p *PacketSend_SaveCombo) Read(buf buffer.BufferRead) {
	p.V1 = make([]types.UShort, 3)
	for i := range p.V1 {
		p.V1[i] = buf.ReadUShort()
	}
}

func (p *PacketSend_SaveCombo) Write(buf buffer.BufferWrite) {
	if len(p.V1) != 3 {
		panic("incorrect length")
	}
	for i := range p.V1 {
		buf.WriteUShort(p.V1[i])
	}
}
