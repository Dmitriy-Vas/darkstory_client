package output

import (
	"darkstory_client/src/buffer"
)

type PacketSend_Cancel struct {
}

func (p *PacketSend_Cancel) Read(_ buffer.BufferRead) {
}

func (p *PacketSend_Cancel) Write(_ buffer.BufferWrite) {
}
