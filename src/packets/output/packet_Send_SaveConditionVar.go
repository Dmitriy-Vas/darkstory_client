package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SaveConditionVar struct {
	V1 []types.String
}

func (p *PacketSend_SaveConditionVar) Read(buf buffer.BufferRead) {
	p.V1 = make([]types.String, 100)
	for i := range p.V1 {
		p.V1[i] = buf.ReadString()
	}
}

func (p *PacketSend_SaveConditionVar) Write(buf buffer.BufferWrite) {
	if len(p.V1) != 100 {
		panic("incorrect length")
	}
	for i := range p.V1 {
		buf.WriteString(p.V1[i])
	}
}
