package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Coupon struct {
	V1 types.String
	V2 types.Boolean
}

func (p *PacketSend_Coupon) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadBoolean()
}

func (p *PacketSend_Coupon) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteBoolean(p.V2)
}
