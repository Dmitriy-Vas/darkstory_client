package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendWhosOnline struct {
}

func (p *PacketSendWhosOnline) Read(_ buffer.BufferRead) {
}

func (p *PacketSendWhosOnline) Write(_ buffer.BufferWrite) {
}
