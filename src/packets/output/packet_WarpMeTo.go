package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketWarpMeTo struct {
	V1 types.String
}

func (p *PacketWarpMeTo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketWarpMeTo) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
