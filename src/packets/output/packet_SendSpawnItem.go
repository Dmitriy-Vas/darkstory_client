package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendSpawnItem struct {
	V1 types.Integer
	V2 types.Integer
}

func (p *PacketSendSpawnItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadInteger()
}

func (p *PacketSendSpawnItem) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteInteger(p.V2)
}
