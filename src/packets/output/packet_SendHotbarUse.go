package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendHotbarUse struct {
}

func (p *PacketSendHotbarUse) Read(_ buffer.BufferRead) {
}

func (p *PacketSendHotbarUse) Write(_ buffer.BufferWrite) {
}
