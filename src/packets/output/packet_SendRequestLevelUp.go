package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendRequestLevelUp struct {
}

func (p *PacketSendRequestLevelUp) Read(_ buffer.BufferRead) {
}

func (p *PacketSendRequestLevelUp) Write(_ buffer.BufferWrite) {
}
