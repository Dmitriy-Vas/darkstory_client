package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_AdminTask struct {
	V1 types.Byte
	V2 types.Integer
	V3 types.String
}

func (p *PacketSend_AdminTask) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadString()
}

func (p *PacketSend_AdminTask) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteString(p.V3)
}
