package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_StopMagicShield struct {
	V1 types.Boolean
}

func (p *PacketSend_StopMagicShield) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
}

func (p *PacketSend_StopMagicShield) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
}
