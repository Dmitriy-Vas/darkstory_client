package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SteamTransaction struct {
	V1 types.Integer
	V2 types.Integer
	V3 types.Long
	V4 types.Boolean
}

func (p *PacketSend_SteamTransaction) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadLong()
	p.V4 = buf.ReadBoolean()
}

func (p *PacketSend_SteamTransaction) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteLong(p.V3)
	buf.WriteBoolean(p.V4)
}
