package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_CloseShop struct {
	V1 types.Boolean
	V2 types.Boolean
}

func (p *PacketSend_CloseShop) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadBoolean()
}

func (p *PacketSend_CloseShop) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteBoolean(p.V2)
}
