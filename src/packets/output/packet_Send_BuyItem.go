package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_BuyItem struct {
	V1 types.Boolean
	V2 types.UShort
	V3 types.Integer
	V4 types.Byte
}

func (p *PacketSend_BuyItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadUShort()
	p.V3 = buf.ReadInteger()
	p.V4 = buf.ReadByte()
}

func (p *PacketSend_BuyItem) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteUShort(p.V2)
	buf.WriteInteger(p.V3)
	buf.WriteByte(p.V4)
}
