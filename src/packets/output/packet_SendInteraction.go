package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendInteraction struct {
	V1 types.Boolean
	V2 types.Byte
	V3 types.Byte
}

func (p *PacketSendInteraction) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadByte()
}

func (p *PacketSendInteraction) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteByte(p.V3)
}
