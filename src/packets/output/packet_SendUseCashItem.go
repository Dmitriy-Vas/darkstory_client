package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendUseCashItem struct {
	V1 types.UShort
}

func (p *PacketSendUseCashItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketSendUseCashItem) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
