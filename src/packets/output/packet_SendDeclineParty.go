package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendDeclineParty struct {
}

func (p *PacketSendDeclineParty) Read(_ buffer.BufferRead) {
}

func (p *PacketSendDeclineParty) Write(_ buffer.BufferWrite) {
}
