package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_UseMegaphone struct {
	V1 types.Byte
	V2 types.String
	V3 types.Byte
	V4 types.UShort
}

func (p *PacketSend_UseMegaphone) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadUShort()
}

func (p *PacketSend_UseMegaphone) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteString(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteUShort(p.V4)
}
