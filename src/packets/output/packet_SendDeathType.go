package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendDeathType struct {
	V1 types.Byte
}

func (p *PacketSendDeathType) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendDeathType) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
