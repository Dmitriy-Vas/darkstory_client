package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendPlayerMove struct {
	V1 types.Byte
	V2 types.Boolean
	V3 types.Vector2
	V4 types.String
	V5 types.Byte
	V6 types.Long
}

func (p *PacketSendPlayerMove) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadBoolean()
	p.V3 = buf.ReadVector2()
	p.V4 = buf.ReadString()
	p.V5 = buf.ReadByte()
	p.V6 = buf.ReadLong()
}

func (p *PacketSendPlayerMove) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteBoolean(p.V2)
	buf.WriteVector2(p.V3)
	buf.WriteString(p.V4)
	buf.WriteByte(p.V5)
	buf.WriteLong(p.V6)
}
