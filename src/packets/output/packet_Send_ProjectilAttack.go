package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_ProjectilAttack struct {
	V1 types.Byte
	V2 types.Byte
	V3 types.Byte
}

func (p *PacketSend_ProjectilAttack) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadByte()
}

func (p *PacketSend_ProjectilAttack) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteByte(p.V3)
}
