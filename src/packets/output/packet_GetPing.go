package output

import (
	"darkstory_client/src/buffer"
)

type PacketGetPing struct {
}

func (p *PacketGetPing) Read(_ buffer.BufferRead) {
}

func (p *PacketGetPing) Write(_ buffer.BufferWrite) {
}
