package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendWarpMap struct {
	V1 types.Integer
}

func (p *PacketSendWarpMap) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
}

func (p *PacketSendWarpMap) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
}
