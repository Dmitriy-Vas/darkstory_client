package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_GetStats struct {
	V1 types.Date
	V2 types.Date
}

func (p *PacketSend_GetStats) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadDate()
	p.V2 = buf.ReadDate()
}

func (p *PacketSend_GetStats) Write(buf buffer.BufferWrite) {
	buf.WriteDate(p.V1)
	buf.WriteDate(p.V2)
}
