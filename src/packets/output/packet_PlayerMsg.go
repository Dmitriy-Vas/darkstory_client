package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketPlayerMsg struct {
	V1 types.String
	V2 types.Byte
}

func (p *PacketPlayerMsg) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadByte()
}

func (p *PacketPlayerMsg) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteByte(p.V2)
}
