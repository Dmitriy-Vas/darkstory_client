package output

import (
	"darkstory_client/src/buffer"
	"sync"
)

var (
	// Кэш пакетов
	packetsCache OutputPackets
	mutex        *sync.RWMutex
)

// Packet описывает пакет и методы для IO взаимодействия
type Packet interface {
	Read(buf buffer.BufferRead)
	Write(buf buffer.BufferWrite)
}

// OutputPackets описывает хранилище для пакетов по их ID или типу
type OutputPackets struct {
	idsMap   map[int]Packet
	typesMap map[interface{}]Packet
}

// GetPacketById возвращает сохранённый пакет по ID
func (p OutputPackets) GetPacketById(id int) Packet {
	return p.idsMap[id]
}

// GetPacketByType возвращает сохранённый пакет с подходящим типом
func (p OutputPackets) GetPacketByType(t interface{}) Packet {
	return p.typesMap[t]
}

// GetPackets создаёт копию из сохранённых пакетов
func GetPackets() OutputPackets {
	mutex.RLock()
	defer mutex.RUnlock()
	packetsCopy := newPackets()
	for key, value := range packetsCache.idsMap {
		packetsCopy.idsMap[key] = value
	}
	for key, value := range packetsCache.typesMap {
		packetsCopy.typesMap[key] = value
	}
	return packetsCopy
}

// При каждом запуске создаётся копия из сохранённых пакетов
func init() {
	packetsCache = newPackets()
	mutex = new(sync.RWMutex)
	registerIds()
	registerTypes()
}

// newPackets возвращает новый экземпляр packets
func newPackets() OutputPackets {
	return OutputPackets{
		idsMap:   make(map[int]Packet),
		typesMap: make(map[interface{}]Packet),
	}
}

// registerIds регистрирует пакеты в соответствии с их ID
func registerIds() {
	mutex.Lock()
	defer mutex.Unlock()

	packetsCache.idsMap[SendAddChar] = (*PacketSendAddChar)(nil)
	packetsCache.idsMap[PlayerMsg] = (*PacketPlayerMsg)(nil)
	packetsCache.idsMap[Send_DragItem] = (*PacketSend_DragItem)(nil)
	packetsCache.idsMap[SayMsg] = (*PacketSayMsg)(nil)
	packetsCache.idsMap[Send_PlayerDir] = (*PacketSendPlayerDir)(nil)
	packetsCache.idsMap[SendUseItem] = (*PacketSendUseItem)(nil)
	packetsCache.idsMap[Send_Hotkeys] = (*PacketSend_Hotkeys)(nil)
	packetsCache.idsMap[Send_PlayerInfoRequest] = (*PacketSend_PlayerInfoRequest)(nil)
	packetsCache.idsMap[WarpMeTo] = (*PacketWarpMeTo)(nil)
	packetsCache.idsMap[WarpToMe] = (*PacketWarpToMe)(nil)
	packetsCache.idsMap[Send_AdminTask] = (*PacketSend_AdminTask)(nil)
	packetsCache.idsMap[Send_UseMegaphone] = (*PacketSend_UseMegaphone)(nil)
	packetsCache.idsMap[Send_ClientRevision] = (*PacketSend_ClientRevision)(nil)
	//packetsCache.idsMap[MapEditorCancel] = (*PacketMapEditorCancel)(nil) // У этого пакета такой же ID как и у следующего (22)
	packetsCache.idsMap[Send_NeedMap] = (*PacketSend_NeedMap)(nil)
	packetsCache.idsMap[Send_MapGetItem] = (*PacketSend_MapGetItem)(nil)
	packetsCache.idsMap[Send_MapDropItem] = (*PacketSend_MapDropItem)(nil)
	packetsCache.idsMap[SendMapRespawn] = (*PacketSendMapRespawn)(nil)
	packetsCache.idsMap[Send_ReportPlayer] = (*PacketSend_ReportPlayer)(nil)
	packetsCache.idsMap[SendBanList] = (*PacketSendBanList)(nil)
	packetsCache.idsMap[SendBan] = (*PacketSendBan)(nil)
	packetsCache.idsMap[SendWhosOnline] = (*PacketSendWhosOnline)(nil)
	packetsCache.idsMap[SendTryOpenUI] = (*PacketSendTryOpenUI)(nil)
	packetsCache.idsMap[Send_Search] = (*PacketSend_Search)(nil)
	packetsCache.idsMap[Send_GetStats] = (*PacketSend_GetStats)(nil)
	packetsCache.idsMap[Send_PlayerCommand] = (*PacketSend_PlayerCommand)(nil)
	packetsCache.idsMap[Send_Button] = (*PacketSend_Button)(nil)
	packetsCache.idsMap[Send_CastSpell] = (*PacketSend_CastSpell)(nil)
	packetsCache.idsMap[Send_TakeCapture] = (*PacketSend_TakeCapture)(nil)
	packetsCache.idsMap[SendSwapSlots] = (*PacketSendSwapSlots)(nil)
	packetsCache.idsMap[GetPing] = (*PacketGetPing)(nil)
	packetsCache.idsMap[SendUnequip] = (*PacketSendUnequip)(nil)
	packetsCache.idsMap[SendSpawnItem] = (*PacketSendSpawnItem)(nil)
	packetsCache.idsMap[Send_TrainStat] = (*PacketSend_TrainStat)(nil)
	packetsCache.idsMap[SendRequestLevelUp] = (*PacketSendRequestLevelUp)(nil)
	packetsCache.idsMap[Send_SetPlayerStoreItem] = (*PacketSend_SetPlayerStoreItem)(nil)
	packetsCache.idsMap[Send_CloseShop] = (*PacketSend_CloseShop)(nil)
	packetsCache.idsMap[Send_SellItem] = (*PacketSend_SellItem)(nil)
	packetsCache.idsMap[ChangeBankSlots] = (*PacketChangeBankSlots)(nil)
	packetsCache.idsMap[Send_DepositItem] = (*PacketSend_DepositItem)(nil)
	packetsCache.idsMap[WithdrawItem] = (*PacketWithdrawItem)(nil)
	packetsCache.idsMap[Send_CloseBank] = (*PacketSend_CloseBank)(nil)
	packetsCache.idsMap[AdminWarp] = (*PacketAdminWarp)(nil)
	packetsCache.idsMap[SendTradeRequest] = (*PacketSendTradeRequest)(nil)
	packetsCache.idsMap[AcceptTrade] = (*PacketAcceptTrade)(nil)
	packetsCache.idsMap[DeclineTrade] = (*PacketDeclineTrade)(nil)
	packetsCache.idsMap[TradeItem] = (*PacketTradeItem)(nil)
	packetsCache.idsMap[UntradeItem] = (*PacketUntradeItem)(nil)
	packetsCache.idsMap[SendRequestUseChar] = (*PacketSendRequestUseChar)(nil)
	packetsCache.idsMap[SendRequestNewChar] = (*PacketSendRequestNewChar)(nil)
	packetsCache.idsMap[SendRequestDelChar] = (*PacketSendRequestDelChar)(nil)
	//packetsCache.idsMap[CheckDefensa] = (*PacketCheckDefensa)(nil) // У этого пакета такой же ID как и у следующего (69)
	packetsCache.idsMap[Send_StopMagicShield] = (*PacketSend_StopMagicShield)(nil)
	packetsCache.idsMap[SendHotbarChange] = (*PacketSendHotbarChange)(nil)
	packetsCache.idsMap[SendHotbarUse] = (*PacketSendHotbarUse)(nil)
	packetsCache.idsMap[Send_Selected] = (*PacketSend_Selected)(nil)
	packetsCache.idsMap[Send_Whisper] = (*PacketSend_Whisper)(nil)
	packetsCache.idsMap[SendReplyPlayerInvitation] = (*PacketSendReplyPlayerInvitation)(nil)
	packetsCache.idsMap[SendDeclineParty] = (*PacketSendDeclineParty)(nil)
	packetsCache.idsMap[SendSaveProjectil] = (*PacketSendSaveProjectil)(nil)
	packetsCache.idsMap[Send_ProjectilAttack] = (*PacketSend_ProjectilAttack)(nil)
	packetsCache.idsMap[SendRequestEditor] = (*PacketSendRequestEditor)(nil)
	packetsCache.idsMap[SendRequestData] = (*PacketSendRequestData)(nil)
	packetsCache.idsMap[SendDeathType] = (*PacketSendDeathType)(nil)
	packetsCache.idsMap[Send_Dig] = (*PacketSend_Dig)(nil)
	packetsCache.idsMap[SendPVP] = (*PacketSendPVP)(nil)
	packetsCache.idsMap[Send_SteamTransaction] = (*PacketSend_SteamTransaction)(nil)
	packetsCache.idsMap[Send_Choice] = (*PacketSend_Choice)(nil)
	packetsCache.idsMap[SendInteraction] = (*PacketSendInteraction)(nil)
	packetsCache.idsMap[SendGetAdminInfo] = (*PacketSendGetAdminInfo)(nil)
	packetsCache.idsMap[SendBreakPoint] = (*PacketSendBreakPoint)(nil)
	packetsCache.idsMap[Send_SaveConditionVar] = (*PacketSend_SaveConditionVar)(nil)
	packetsCache.idsMap[Send_SaveCombo] = (*PacketSend_SaveCombo)(nil)
	packetsCache.idsMap[Send_LoginBack] = (*PacketSend_LoginBack)(nil)
	packetsCache.idsMap[SendWarpMap] = (*PacketSendWarpMap)(nil)
	packetsCache.idsMap[Send_LogOut] = (*PacketSend_LogOut)(nil)
	packetsCache.idsMap[SendUseCashItem] = (*PacketSendUseCashItem)(nil)
	packetsCache.idsMap[Send_GetPlayerInfo] = (*PacketSend_GetPlayerInfo)(nil)
	packetsCache.idsMap[Send_UpdatePlayerHonor] = (*PacketSend_UpdatePlayerHonor)(nil)
	packetsCache.idsMap[SendDropCalaveras] = (*PacketSendDropCalaveras)(nil)
	packetsCache.idsMap[Send_Coupon] = (*PacketSend_Coupon)(nil)
	packetsCache.idsMap[SendCashShopUI] = (*PacketSendCashShopUI)(nil)
	packetsCache.idsMap[Send_FavAwards] = (*PacketSend_FavAwards)(nil)
	packetsCache.idsMap[Send_SelectMinigames] = (*PacketSend_SelectMinigames)(nil)
	packetsCache.idsMap[SendDemoValue] = (*PacketSendDemoValue)(nil)
	packetsCache.idsMap[Send_ForgeAction] = (*PacketSend_ForgeAction)(nil)
	packetsCache.idsMap[Send_Cancel] = (*PacketSend_Cancel)(nil)
}

// registerTypes регистрирует пакеты в соответствии с их типом
func registerTypes() {
	// TODO fill
}

// Список идентификаторов пакетов
const (
	SendNewAccount = 1
	SendLogin      = 2
	SendAddChar    = 3

	PlayerMsg              = 5
	Send_ProfessionEnd     = 6
	Send_DragItem          = 7
	SayMsg                 = 8
	TrySendPlayerMove      = 9
	Send_PlayerDir         = 10
	SendUseItem            = 11
	Send_Attack            = 12
	Send_Hotkeys           = 13
	Send_PlayerInfoRequest = 14
	WarpMeTo               = 15
	WarpToMe               = 16
	Send_AdminTask         = 17
	Send_ChangePassword    = 18
	Send_UseMegaphone      = 19
	Send_ClientRevision    = 20
	Send_MapData           = 21

	// Я не знаю почему у двух пакетов одинаковый ID :<
	MapEditorCancel = 22
	Send_NeedMap    = 22

	Send_MapGetItem   = 23
	Send_MapDropItem  = 24
	SendMapRespawn    = 25
	Send_ReportPlayer = 26
	SendBanList       = 27
	Send_Enhancement  = 28
	SendBan           = 29
	SendSaveItem      = 30
	SendSaveNpc       = 31
	SendSaveShop      = 32
	SendSaveSpell     = 33

	SendWhosOnline     = 35
	SendTryOpenUI      = 36
	Send_Search        = 37
	Send_GetStats      = 38
	Send_PlayerCommand = 39
	Send_Button        = 40

	Send_CastSpell          = 42
	Send_TakeCapture        = 43
	SendSwapSlots           = 44
	SendSaveResource        = 45
	GetPing                 = 46
	SendUnequip             = 47
	SendSpawnItem           = 48
	Send_TrainStat          = 49
	SendSaveAnimation       = 50
	SendRequestLevelUp      = 51
	Send_SetPlayerStoreItem = 52
	Send_CloseShop          = 53
	Send_BuyItem            = 54
	Send_SellItem           = 55
	ChangeBankSlots         = 56
	Send_DepositItem        = 57
	WithdrawItem            = 58
	Send_CloseBank          = 59
	AdminWarp               = 60
	SendTradeRequest        = 61
	AcceptTrade             = 62
	DeclineTrade            = 63
	TradeItem               = 64
	UntradeItem             = 65
	SendRequestUseChar      = 66
	SendRequestNewChar      = 67
	SendRequestDelChar      = 68

	// Я не знаю почему у двух пакетов одинаковый ID :<
	Send_StopMagicShield = 69
	CheckDefensa         = 69

	SendChangeSpellSlots      = 70
	SendHotbarChange          = 71
	SendHotbarUse             = 72
	Send_SaveQuest            = 73
	Send_Selected             = 74
	Send_Whisper              = 75
	SendPartyRequest          = 76
	SendReplyPlayerInvitation = 77
	SendDeclineParty          = 78

	SendSaveProjectil    = 80
	Send_ProjectilAttack = 81
	Send_SaveMoral       = 82
	SendRequestEditor    = 83
	SendSaveCard         = 84
	SendRequestData      = 85
	Send_SaveCrafting    = 86
	Send_Crafting        = 87

	SendDeathType = 89

	Send_Dig               = 91
	SendPVP                = 92
	SendSaveMapTopo        = 93
	Send_SteamTransaction  = 94
	SendSaveCondition      = 95
	Send_Choice            = 96
	SendInteraction        = 97
	SendGetAdminInfo       = 98
	SendBreakPoint         = 99
	Send_SaveConditionVar  = 100
	SendSavePuzzle         = 101
	Send_SaveCombo         = 102
	Send_LoginBack         = 103
	SendWarpMap            = 104
	Send_LogOut            = 105
	SendUseCashItem        = 106
	Send_GetPlayerInfo     = 107
	Send_UpdatePlayerHonor = 108
	SendUIBuddies          = 109
	SendDropCalaveras      = 110
	Send_Coupon            = 111
	SendCashShopUI         = 112
	Send_FavAwards         = 113
	Send_SelectMinigames   = 114
	SendDemoValue          = 115
	Send_ForgeAction       = 116
	Send_Cancel            = 117
	Send_ResetPassword     = 118
	Send_GetUsername       = 119
)
