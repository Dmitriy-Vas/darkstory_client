package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendSaveProjectil struct {
	V1 types.Integer
	V2 types.String
	V3 types.Integer
	V4 types.Integer
	V5 types.Integer
	V6 types.Integer
	V7 types.Integer
	V8 types.Boolean
	V9 []types.Integer
}

func (p *PacketSendSaveProjectil) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadInteger()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadInteger()
	p.V4 = buf.ReadInteger()
	p.V5 = buf.ReadInteger()
	p.V6 = buf.ReadInteger()
	p.V7 = buf.ReadInteger()
	p.V8 = buf.ReadBoolean()
	p.V9 = make([]types.Integer, 5)
	for i := range p.V9 {
		p.V9[i] = buf.ReadInteger()
	}
}

func (p *PacketSendSaveProjectil) Write(buf buffer.BufferWrite) {
	buf.WriteInteger(p.V1)
	buf.WriteString(p.V2)
	buf.WriteInteger(p.V3)
	buf.WriteInteger(p.V4)
	buf.WriteInteger(p.V5)
	buf.WriteInteger(p.V6)
	buf.WriteInteger(p.V7)
	buf.WriteBoolean(p.V8)
	if len(p.V9) != 5 {
		panic("incorrect length")
	}
	for i := range p.V9 {
		buf.WriteInteger(p.V9[i])
	}
}
