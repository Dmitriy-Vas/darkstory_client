package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendGetAdminInfo struct {
	V1 types.Byte
}

func (p *PacketSendGetAdminInfo) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendGetAdminInfo) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
