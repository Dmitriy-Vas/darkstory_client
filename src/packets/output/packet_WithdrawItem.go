package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketWithdrawItem struct {
	V1 types.UShort
	V2 types.Long
}

func (p *PacketWithdrawItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
	p.V2 = buf.ReadLong()
}

func (p *PacketWithdrawItem) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
	buf.WriteLong(p.V2)
}
