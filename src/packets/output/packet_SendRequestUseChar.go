package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendRequestUseChar struct {
	V1 types.Byte
	V2 types.String
	V3 types.Boolean
}

func (p *PacketSendRequestUseChar) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadString()
	p.V3 = buf.ReadBoolean()
}

func (p *PacketSendRequestUseChar) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteString(p.V2)
	buf.WriteBoolean(p.V3)
}
