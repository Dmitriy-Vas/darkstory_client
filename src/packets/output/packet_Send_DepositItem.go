package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_DepositItem struct {
	V1 types.Byte
	V2 types.Long
	V3 types.Boolean
}

func (p *PacketSend_DepositItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadLong()
	p.V3 = buf.ReadBoolean()
}

func (p *PacketSend_DepositItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteLong(p.V2)
	buf.WriteBoolean(p.V3)
}
