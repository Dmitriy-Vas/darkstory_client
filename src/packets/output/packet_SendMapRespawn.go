package output

import (
	"darkstory_client/src/buffer"
)

type PacketSendMapRespawn struct {
}

func (p *PacketSendMapRespawn) Read(_ buffer.BufferRead) {
}

func (p *PacketSendMapRespawn) Write(_ buffer.BufferWrite) {
}
