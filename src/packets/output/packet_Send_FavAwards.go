package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_FavAwards struct {
	V1 types.Boolean
	V2 types.UShort
}

func (p *PacketSend_FavAwards) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadUShort()
}

func (p *PacketSend_FavAwards) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteUShort(p.V2)
}
