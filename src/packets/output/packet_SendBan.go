package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendBan struct {
	V1 types.String
}

func (p *PacketSendBan) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
}

func (p *PacketSendBan) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
}
