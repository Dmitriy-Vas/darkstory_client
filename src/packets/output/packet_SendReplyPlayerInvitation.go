package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendReplyPlayerInvitation struct {
	V1 types.Byte
	V2 types.Boolean
}

func (p *PacketSendReplyPlayerInvitation) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadBoolean()
}

func (p *PacketSendReplyPlayerInvitation) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteBoolean(p.V2)
}
