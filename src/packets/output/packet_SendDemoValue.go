package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendDemoValue struct {
	V1 types.Byte
	V2 types.Integer
	V3 types.Integer
}

func (p *PacketSendDemoValue) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadInteger()
	p.V3 = buf.ReadInteger()
}

func (p *PacketSendDemoValue) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteInteger(p.V2)
	buf.WriteInteger(p.V3)
}
