package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_SellItem struct {
	V1 types.Byte
	V2 types.Long
}

func (p *PacketSend_SellItem) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
	p.V2 = buf.ReadLong()
}

func (p *PacketSend_SellItem) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
	buf.WriteLong(p.V2)
}
