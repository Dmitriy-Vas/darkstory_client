package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendRequestDelChar struct {
	V1 types.Byte
}

func (p *PacketSendRequestDelChar) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadByte()
}

func (p *PacketSendRequestDelChar) Write(buf buffer.BufferWrite) {
	buf.WriteByte(p.V1)
}
