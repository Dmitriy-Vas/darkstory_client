package output

import (
	"darkstory_client/src/buffer"
)

type PacketSend_LoginBack struct {
}

func (p *PacketSend_LoginBack) Read(_ buffer.BufferRead) {
}

func (p *PacketSend_LoginBack) Write(_ buffer.BufferWrite) {
}
