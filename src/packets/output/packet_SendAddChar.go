package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendAddChar struct {
	V1 types.String
	V2 types.Byte
	V3 types.Byte
	V4 types.UShort
	V5 types.UShort
	V6 types.UShort
	V7 types.String
}

func (p *PacketSendAddChar) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadString()
	p.V2 = buf.ReadByte()
	p.V3 = buf.ReadByte()
	p.V4 = buf.ReadUShort()
	p.V5 = buf.ReadUShort()
	p.V6 = buf.ReadUShort()
	p.V7 = buf.ReadString()
}

func (p *PacketSendAddChar) Write(buf buffer.BufferWrite) {
	buf.WriteString(p.V1)
	buf.WriteByte(p.V2)
	buf.WriteByte(p.V3)
	buf.WriteUShort(p.V4)
	buf.WriteUShort(p.V5)
	buf.WriteUShort(p.V6)
	buf.WriteString(p.V7)
}
