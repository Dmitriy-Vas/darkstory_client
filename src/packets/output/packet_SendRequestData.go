package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSendRequestData struct {
	V1 types.UShort
}

func (p *PacketSendRequestData) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadUShort()
}

func (p *PacketSendRequestData) Write(buf buffer.BufferWrite) {
	buf.WriteUShort(p.V1)
}
