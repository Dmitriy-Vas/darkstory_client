package output

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type PacketSend_Choice struct {
	V1 types.Boolean
	V2 types.Byte
	V3 types.String
	V4 types.Byte
	V5 types.UShort
	V6 types.Byte
}

func (p *PacketSend_Choice) Read(buf buffer.BufferRead) {
	p.V1 = buf.ReadBoolean()
	p.V2 = buf.ReadByte()
	if p.V1 {
		p.V3 = buf.ReadString()
	}
	p.V4 = buf.ReadByte()
	p.V5 = buf.ReadUShort()
	p.V6 = buf.ReadByte()
}

func (p *PacketSend_Choice) Write(buf buffer.BufferWrite) {
	buf.WriteBoolean(p.V1)
	buf.WriteByte(p.V2)
	if p.V1 {
		buf.WriteString(p.V3)
	}
	buf.WriteByte(p.V4)
	buf.WriteUShort(p.V5)
	buf.WriteByte(p.V6)
}
