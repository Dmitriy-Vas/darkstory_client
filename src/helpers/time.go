package helpers

import "time"

func ConvertStrToDate(str string) time.Time {
	t, _ := time.Parse("02/01/2006", str)
	return t
}
