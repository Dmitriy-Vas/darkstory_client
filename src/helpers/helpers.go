package helpers

import (
	"errors"
	"strconv"
)

func NotValid(value int, max int) bool {
	return value <= 0 || value > max
}

func IsNumeric(str string) bool {
	_, err1 := strconv.ParseInt(str, 0, 64)
	_, err2 := strconv.ParseFloat(str, 64)
	return err1 == nil ||
		err2 == nil ||
		errors.Is(err1, strconv.ErrRange) ||
		errors.Is(err2, strconv.ErrRange)
}
