package helpers

import "testing"

func TestIsNumeric(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "0x64",
			args: args{
				str: "0x64",
			},
			want: true,
		},
		{
			name: "100",
			args: args{
				str: "100",
			},
			want: true,
		},
		{
			name: "50.123",
			args: args{
				str: "50.123",
			},
			want: true,
		},
		{
			name: "Test",
			args: args{
				str: "Test",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsNumeric(tt.args.str); got != tt.want {
				t.Errorf("IsNumeric() = %v, want %v", got, tt.want)
			}
		})
	}
}
