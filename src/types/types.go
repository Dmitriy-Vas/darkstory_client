package types

import "time"

type (
	Vector2 struct {
		X Integer
		Y Integer
	}

	Bytes       = []byte
	String      = string
	StringArray = []String
	Date        = time.Time
	Boolean     = bool

	SByte = int8
	Byte  = uint8

	Short  = int16
	UShort = uint16

	Integer  = int32
	UInteger = uint32

	Long  = int64
	ULong = uint64
)
