package buffer

import "darkstory_client/src/types"

type buffer struct {
}

// BufferRead описывает методы для считывания данных
type BufferRead interface {
	ReadVector2() types.Vector2
	ReadBytes() types.Bytes
	ReadString() types.String
	ReadStringArray() types.StringArray
	ReadDate() types.Date
	ReadBoolean() types.Boolean
	ReadSByte() types.SByte
	ReadByte() types.Byte
	ReadShort() types.Short
	ReadUShort() types.UShort
	ReadInteger() types.Integer
	ReadUInteger() types.UInteger
	ReadLong() types.Long
	ReadULong() types.ULong
}

// BufferWrite описывает методы для записи данных
type BufferWrite interface {
	WriteVector2(types.Vector2)
	WriteBytes(types.Bytes)
	WriteString(types.String)
	WriteStringArray(types.StringArray)
	WriteDate(types.Date)
	WriteBoolean(types.Boolean)
	WriteSByte(types.SByte)
	WriteByte(types.Byte)
	WriteShort(types.Short)
	WriteUShort(types.UShort)
	WriteInteger(types.Integer)
	WriteUInteger(types.UInteger)
	WriteLong(types.Long)
	WriteULong(types.ULong)
}
