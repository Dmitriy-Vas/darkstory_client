package buffer

import "darkstory_client/src/types"

func (b *buffer) ReadVector2() types.Vector2 {
	panic("implement me")
}

func (b *buffer) ReadBytes() types.Bytes {
	panic("implement me")
}

func (b *buffer) ReadString() types.String {
	panic("implement me")
}

func (b *buffer) ReadStringArray() types.StringArray {
	panic("implement me")
}

func (b *buffer) ReadDate() types.Date {
	panic("implement me")
}

func (b *buffer) ReadBoolean() types.Boolean {
	panic("implement me")
}

func (b *buffer) ReadSByte() types.SByte {
	panic("implement me")
}

func (b *buffer) ReadByte() types.Byte {
	panic("implement me")
}

func (b *buffer) ReadShort() types.Short {
	panic("implement me")
}

func (b *buffer) ReadUShort() types.UShort {
	panic("implement me")
}

func (b *buffer) ReadInteger() types.Integer {
	panic("implement me")
}

func (b *buffer) ReadUInteger() types.UInteger {
	panic("implement me")
}

func (b *buffer) ReadLong() types.Long {
	panic("implement me")
}

func (b *buffer) ReadULong() types.ULong {
	panic("implement me")
}
