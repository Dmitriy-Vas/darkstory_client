package buffer

import (
	"darkstory_client/src/types"
)

func (b *buffer) WriteVector2(value types.Vector2) {
	panic("implement me")
}

func (b *buffer) WriteBytes(value types.Bytes) {
	panic("implement me")
}

func (b *buffer) WriteString(value types.String) {
	panic("implement me")
}

func (b *buffer) WriteStringArray(value types.StringArray) {
	panic("implement me")
}

func (b *buffer) WriteDate(value types.Date) {
	panic("implement me")
}

func (b *buffer) WriteBoolean(value types.Boolean) {
	panic("implement me")
}

func (b *buffer) WriteSByte(value types.SByte) {
	panic("implement me")
}

func (b *buffer) WriteByte(value types.Byte) {
	panic("implement me")
}

func (b *buffer) WriteShort(value types.Short) {
	panic("implement me")
}

func (b *buffer) WriteUShort(value types.UShort) {
	panic("implement me")
}

func (b *buffer) WriteInteger(value types.Integer) {
	panic("implement me")
}

func (b *buffer) WriteUInteger(value types.UInteger) {
	panic("implement me")
}

func (b *buffer) WriteLong(value types.Long) {
	panic("implement me")
}

func (b *buffer) WriteULong(value types.ULong) {
	panic("implement me")
}
