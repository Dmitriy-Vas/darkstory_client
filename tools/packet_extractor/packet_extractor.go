package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

const (
	ByteBuffer_ValueName   = "byteBuffer"
	InputPacketNameRegexp  = `[p|P]acket_(\w+)`
	OutputPacketNameRegexp = `void (\w+)`
	ReadRegexp             = `\.Read(\w+)`
	WriteRegexp            = `\.Write(\w+)`
	InputFile              = "input.cs"
)

func main() {
	makeOutputPacket()
}

func getPacketName(data []byte, rxp *regexp.Regexp) (packetName string) {
	packetName = "Packet"
	if nameMatches := rxp.FindStringSubmatch(string(data)); nameMatches != nil {
		packetName = nameMatches[1]
	}
	return packetName
}

func getPacketData(data []byte, rxp *regexp.Regexp) (
	variables string,
	read string,
	write string,
) {
	var variablesBuf bytes.Buffer
	var readBuf bytes.Buffer
	var writeBuf bytes.Buffer

	subMatches := rxp.FindAllStringSubmatch(string(data), -1)
	for i, submatch := range subMatches {
		if i == 0 {
			continue
		}
		valueType := submatch[1]
		readBuf.WriteString(fmt.Sprintf("\tp.V%d = buf.Read%s()\n", i, valueType))
		writeBuf.WriteString(fmt.Sprintf("\tbuf.Write%s(p.V%d)\n", valueType, i))
		variablesBuf.WriteString(fmt.Sprintf("\tV%d types.%s\n", i, valueType))
	}
	return variablesBuf.String(), readBuf.String(), writeBuf.String()
}

func makeOutputPacket() {
	writeRegexp := regexp.MustCompile(ByteBuffer_ValueName + WriteRegexp)
	packetRegexp := regexp.MustCompile(OutputPacketNameRegexp)

	data, err := ioutil.ReadFile(InputFile)
	if err != nil {
		panic(err)
	}

	packetName := getPacketName(data, packetRegexp)
	variables, read, write := getPacketData(data, writeRegexp)

	MakePacket(
		"output",
		packetName,
		variables,
		read,
		write,
	)
}

func makeInputPacket() {
	readRegexp := regexp.MustCompile(ByteBuffer_ValueName + ReadRegexp)
	packetRegexp := regexp.MustCompile(InputPacketNameRegexp)

	data, err := ioutil.ReadFile(InputFile)
	if err != nil {
		panic(err)
	}

	packetName := getPacketName(data, packetRegexp)
	variables, read, write := getPacketData(data, readRegexp)

	MakePacket(
		"input",
		packetName,
		variables,
		read,
		write,
	)
}

func MakePacket(
	packageName string,
	packetName string,
	variables string,
	read string,
	write string,
) {
	packet := strings.NewReplacer(
		"${GO_PACKAGE_NAME}", packageName,
		"${PACKET_NAME}", packetName,
		"${PACKET_VARIABLES}", variables,
		"${PACKET_READ}", read,
		"${PACKET_WRITE}", write,
	).Replace(PacketTemplate)
	if err := ioutil.WriteFile(fmt.Sprintf("packet_%s.go", packetName), []byte(packet), 0666); err != nil {
		panic(err)
	}
}

const PacketTemplate = `package ${GO_PACKAGE_NAME}

import (
	"darkstory_client/src/buffer"
	"darkstory_client/src/types"
)

type Packet${PACKET_NAME} struct {
${PACKET_VARIABLES}}

func (p *Packet${PACKET_NAME}) Read(buf buffer.BufferRead) {
${PACKET_READ}}

func (p *Packet${PACKET_NAME}) Write(buf buffer.BufferWrite) {
${PACKET_WRITE}}
`
